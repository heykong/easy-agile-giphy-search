# Easy Agile Giphy Search
------

## Guideline

### Do
- Must use React, TypeScript is not a requirement
- Creating a working search interface to display Giphy search results which you feel is nice to use
- Use any of the npm modules you want use to create this app
- Feel free to use a react component library like `material-ui`
- Do make the API requests and parse the results yourself
- Choose one of CSS options or add your own if you're more familiar with it. i.e. `styled-components` and remove the rest so there's no confusion

### Don't
- Don't write everything from scratch
- Don't reinvent the wheel
- Don't use the giphy npm module
- Don't work on the back-end infrastructure

### What is important

- It is intuitive to use   
*What you think makes an app feel good to use, think about great experience you've had and try to bring a bit of that to your work*
- It loads quickly
- It feels quick to use
- It handles errors nicely

----
## Giphy API

### Key
`iWlVn6Uk7AztzOsYbObVqTtoEY1qqaRU`

### API Endpoint
[Endpoint](https://developers.giphy.com/docs/api/endpoint#search)<br>
**Example:** `https://api.giphy.com/v1/gifs/search?api_key=iWlVn6Uk7AztzOsYbObVqTtoEY1qqaRU&q=awesome`

---
## Fetch API
[MDN](https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API)