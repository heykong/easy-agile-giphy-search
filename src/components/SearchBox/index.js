import React, {useEffect, useState, useRef} from 'react'
import PropTypes from 'prop-types'
import Button from '@material-ui/core/Button'
import TextField from '@material-ui/core/TextField'
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import SearchIcon from '@material-ui/icons/Search';
import TrendingUpIcon from '@material-ui/icons/TrendingUp';
import GifIcon from '@material-ui/icons/Gif';
import DeleteOutlineIcon from '@material-ui/icons/DeleteOutline';
import Switch from '@material-ui/core/Switch';
import FormControlLabel from '@material-ui/core/FormControlLabel';

import styles from './styles.css'

const SearchBox = ({setList, setAnimate, setLoading}) => {
  const searchInput = useRef();
  const [keyword, setKeyword] = useState('');
  const [showOptions, setShowOptions] = useState(false);
  const [animateOnHover, setAnimateOnHover] = useState(false);

  useEffect(() => {
      setAnimate(!animateOnHover);
  }, [animateOnHover]);

  async function fetchGifs (api) {
      setLoading(true);
      const response = await fetch(api);
      const result = await response.json();
      if(result.meta.status == '200') {
        setList({
          data: result.data, 
          success: true, 
          done: true, 
          animate: !animateOnHover
        })
      } else {
        setList({
          data: result.data,
          success: false, 
          done: true, 
          animate: !animateOnHover
        })
      }
      setLoading(false);
  }

  const handleInputChange = (e) => {
    setKeyword(e.target.value)
  }

  const handleSwitch = (e) => {
    setAnimateOnHover(e.target.checked);
  }

  const handleSubmit = (e) => {
    e.preventDefault();
    const apiUrl = `https://api.giphy.com/v1/gifs/search?api_key=iWlVn6Uk7AztzOsYbObVqTtoEY1qqaRU&q=${keyword}`;
    setList([]);
    fetchGifs(apiUrl);
    setShowOptions(true);
  }

  const showTrending = () => {
    const apiUrl = `https://api.giphy.com/v1/gifs/trending?api_key=iWlVn6Uk7AztzOsYbObVqTtoEY1qqaRU`;
    setList([]);
    fetchGifs(apiUrl);
    setShowOptions(true);
  }

  const clearSearch = () => {
    setList({});
    setKeyword('');
    searchInput.current.focus();
    setShowOptions(false);
  }

  return (
    <>
      <form className={styles.form} onSubmit={handleSubmit}>
        <Grid container spacing={0} justify="flex-start" alignItems="flex-end" className={styles.searchBox}>
          <Grid item className={styles.gifIcon}>
            <GifIcon color="secondary" />
          </Grid>
          <Grid item xs>
            <TextField 
              className={styles.input} 
              inputRef={searchInput} 
              type="search" 
              required
              placeholder="Start typing to search" 
              InputProps={{ disableUnderline: true }} 
              autoComplete="off"
              value={keyword}
              onChange={handleInputChange}
            />
          </Grid>
          <Grid item>
            <Button type="submit" variant="contained" color="secondary">
                <SearchIcon className={styles.searchIcon}/>
                Search
            </Button>
          </Grid>
        </Grid>
        </form>
        { showOptions && 
          <Box className={styles.options}>
            <Grid container spacing={2} direction="row" justify="flex-end" alignItems="center">
              <Grid item>
                <FormControlLabel
                  control={
                    <Switch
                      checked={animateOnHover}
                      onChange={handleSwitch}
                      color="secondary"
                      inputProps={{ 'aria-label': 'animation on hover checkbox' }}
                    />
                  }
                  label="ANIMATE ON HOVER" 
                />
              </Grid>
              <Grid item>
                <Button onClick={showTrending} variant="contained" color="primary">
                  <TrendingUpIcon className={styles.trendingIcon}/>
                  trending
                </Button>
              </Grid>
              <Grid item>
                <Button className={styles.button} onClick={clearSearch} variant="contained" color="default">
                  <DeleteOutlineIcon className={styles.clearIcon}/>
                  Clear
                </Button>
              </Grid>
            </Grid>
          </Box>
        }
    </>
  )
}

SearchBox.propTypes = {
  setList: PropTypes.func,
  setAnimate: PropTypes.func,
  setLoading: PropTypes.func
};

export default SearchBox;
