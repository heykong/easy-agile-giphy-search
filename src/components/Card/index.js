import React, {useState} from 'react'
import PropTypes from 'prop-types';

import styles from './styles.css'

const Card = ({ data }) => {
  const still = data.images['downsized_still'].url;
  const animated = data.images['downsized_medium'].url;
  const original = data.images.original.url;
  
  const [hoverImg, setHoverImg] = useState(still);
  
  const handleMouseEnter = () => {
    setHoverImg(animated);
  }

  const handleMouseLeave = () => {
    setHoverImg(still);
  }

  return (
    <div className={styles.card} key={data.id} onMouseEnter={handleMouseEnter} onMouseLeave={handleMouseLeave}>
      { data.animate ? (
        <img className={styles.img} alt={data.title} data-original={original} src={animated}/>
      ) : (
        <img className={styles.img} alt={data.title} data-original={original} src={hoverImg}/>
      )}
      { data.title && <span children={data.title} /> }
    </div>
  )
}

Card.propTypes = {
  data: PropTypes.object
};

export default Card;
