import React, { useState } from 'react';
import { Masonry } from 'masonic';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import CancelIcon from '@material-ui/icons/Cancel';

import Card from '../Card'

import styles from './styles.css'

const SearchResults = ({ list }) => {
  const [modal, setModal] = useState({open: false, img: ''});

  const handleMasonryClick = (e) => {
    if(e.target.nodeName === 'IMG' && e.target.dataset.original !== '') {
      setModal({
        open: true,
        img: e.target.dataset.original
      });
    }
  }

  const closeModal = () => {
    if(!modal.open) return;
    setModal({open: false, img: ''});
  }

  return (
    <div className={styles.container}>
      {!list.success ? (
        <div>Whoops, something unexpected happend when trying to get you fresh gifs.</div>
      ) : (
        list.data.length <= 0 ? (
          <div>
            <p>Bad luck, there is no gif matching your search keyword.</p>
            <p>Click the trending button above to check some trending gifs.</p>
          </div>
        ) : (
          <div onClick={handleMasonryClick}>
            <div className={styles.message}>Showing <strong>{list.data.length}</strong> gifs</div>
            <Masonry
              items={list.data.map(item=> ({...item, animate:list.animate}))}
              columnGutter={20}
              overscanBy={20}
              render={Card}
            />
            <div className={classnames(styles.modal, {
              [styles.modalOpen]: modal.open
            })}>
              <CancelIcon className={styles.closeIcon} onClick={closeModal}/>
              <img src={modal.img} className={styles.modalImage}/>
            </div>
          </div>
        )
      )}
    </div>
  )
}

SearchResults.propTypes = {
  list: PropTypes.object
};

export default SearchResults;
