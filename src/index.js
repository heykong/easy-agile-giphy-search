import React, { useEffect, useState } from "react";
import ReactDOM from "react-dom";
import GifIcon from '@material-ui/icons/Gif';
import Grid from '@material-ui/core/Grid';

import SearchBox from './components/SearchBox';
import ColorTheme from './ColorTheme';
import SearchResults from './components/SearchResults';


import styles from "./styles.css";

const App = () => {
  const [list, setList] = useState({});
  const [animate, setAnimate] = useState(true);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    setList(prevList => ({
      data: prevList.data,
      success: prevList.success,
      done: prevList.done,
      animate: animate,
    }))
  }, [animate])

  return (
    <ColorTheme>
      <div className={styles.container}>
        <div className={styles.search}>
          <SearchBox className={styles.search} setList={setList} setAnimate={setAnimate} setLoading={setLoading}/>
        </div>
        {loading && (
          <Grid container direction="column" alignItems="center" justify="flex-start" className={styles.loading}>
            <GifIcon color="secondary" className={styles.loadingGif}/>
            <p>Loading...</p>
          </Grid>
        )}
        {list.done && 
        <div className={styles.result}>
           <SearchResults list={list} />
        </div>}
      </div>
    </ColorTheme>
  );
}

var mountNode = document.getElementById("app");
ReactDOM.render(<App />, mountNode);