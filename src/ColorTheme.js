import React from 'react';
import { createMuiTheme } from '@material-ui/core/styles';
import { ThemeProvider } from '@material-ui/styles';

const theme = createMuiTheme({
  typography: {
    htmlFontSize: 16,
    fontFamily: "Asap, sans-serif",
  },
  palette: {
    primary: {
      main: '#273756',
      contrastText: '#e0e1dd',
    },
    secondary: {
      main: '#ff9b42'
    },
    default: {
      main: '#888888'
    }
  }
});

export default function ColorTheme(props) {
  return (
    <ThemeProvider theme={theme}>
      {props.children}
    </ThemeProvider>
  );
}